class My_list(list):
    def __init__(self, x, y, z):
        super().__init__([x, y, z])
        self.x = x
        self.y = y
        self.z = z
        self.spisok = [x, y, z]
    def __sub__(self, other):
        return My_list(self.x - other.x, self.y - other.y, self.z - other.z)
    def __truediv__(self, other):
        return My_list(self.x/other, self.y/other, self.z/other)
    def __str__(self):
        return super().__str__()
l1 = My_list(1, 2, 3)
l2 = My_list(3, 4, 5)
l3 = l2 - l1
l4 = l1 - l2
print(l1 / 5)
print(l3)
print(l4) 
