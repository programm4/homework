import random
import tkinter as tk
from tkinter import messagebox
while True:
    try:
        number = int(input('Введите номер задачи от 1 до 16: '))
        if number < 1 or number > 16:
            print('Введите правильный номер задачи!')
        if number == 1:
            print(
                'Cоставить программу обмена значениями трех переменных а, b, c: так, чтобы b присвоилось значение c, с присвоить значение а, a присвоить значение b.')
            a, b, c = 10, 20, 30
            print('a =', a, 'b =', b, 'c =', c)
            a, b, c = b, c, a
            print('a =', a, 'b =', b, 'c =', c)
        if number == 2:
            print('Пользователь вводит 2 числа. Когда пользователь ввел числа, выведите сумму этих чисел.')
            while True:
                try:
                    n = int(input("Введите количество чисел: "))
                    sum = 0
                    for i in range(n):
                        while True:
                            try:
                                a = int(input("Введите число: "))
                                sum += a
                                break
                            except ValueError:
                                print("Вводите числа!")
                    print(sum)
                    break
                except ValueError:
                    print("Вводите числа!")
        if number == 3:
            print('Дано число x, которое принимает значение от 0 до 100. Вычислите чему будет равно x^5')
            fl = True
            while fl:
                try:
                    x = int(input("Введите число от 0 о 100: "))
                    if x < 0 or x > 100:
                        print('Вы ввели неправильное число!')
                        continue
                    y = x
                    for i in range(4):
                        x *= y
                    print('x^5 =', x)
                    fl = False
                except ValueError:
                    print('Вводите числа!')
        if number == 4:
            print('Проверьте, принадлежит ли введенное число числам Фибоначчи.')
            fl = True
            while fl:
                try:
                    x = int(input('Введите число от 0 до 250: '))
                    fib1 = 0
                    fib2 = 1
                    if x < 0 or x > 250:
                        print('Вы ввели неправильное число!')
                        continue
                    while fib2 < x:
                        fib1, fib2 = fib2, fib1 + fib2
                    if fib2 == x:
                        print('Данное число принадлежит к числам Фиббоначи')
                    else:
                        print('Данное число не принадлежит к числам Фиббоначи')
                    fl = False
                except ValueError:
                    print('Вводите числа!')
        if number == 5:
            print(
                'Реализуйте программу двумя способами на определение времени года в зависимости от введенного месяца года.')
            fl = True
            print("Первый способ:")
            while fl:
                try:
                    d = {"январь": "зима", "февраль": "зима", "март": "весна", "апрель": "весна", "май": "весна",
                         "июнь": "лето",
                         "июль": "лето", "август": "лето", "сентябрь": "осень", "октябрь": "осень", "ноябрь": "осень",
                         "декабрь": "зима"}
                    print(d[input("Введите название месяца:")])
                    fl = False
                except KeyError:
                    print('Вы неправильно ввели месяц')

            print("Второй способ:")
            fl2 = True
            while fl2:
                try:
                    n = int(input("Введите номер месяца:"))
                    if n == 12 or n <= 2:
                        print("зима")
                    elif n >= 3 and n <= 5:
                        print("весна")
                    elif n >= 6 and n <= 8:
                        print("лето")
                    elif n >= 9 and n <= 11:
                        print("осень")
                    else:
                        print("Вы ввели неправильное число (((")
                        continue
                    fl2 = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 6:
            print('Посчитайте сумму, количество четных и нечетных чисел от 1 до N. N вводит пользователь.')
            fl = True
            while fl:
                try:
                    n = int(input("Введите число "))
                    if n % 2 == 0:
                        print("Количество четных чисел: ", n // 2)
                        print("Количество нечетных чисел:", n // 2)
                    else:
                        print("Количество четных чисел: ", n // 2)
                        print("Количество нечетных чисел:", n // 2 + 1)
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 7:
            print(
                'Для каждого из чисел от 1 до N, где N меньше 250 выведите количество делителей. N вводит пользователь. Выведите число и через пробел количество его делителей')
            fl = True
            while fl:
                try:
                    n = int(input("Введите число от 1 до 250: "))
                    if n < 1 or n > 250:
                        print('Вы ввели неправильное число')
                        continue
                    c = 0
                    for i in range(1, n + 1):
                        if n % i == 0:
                            c += 1
                    fl = False
                    print('Количество делителей числа', n, '-', c)
                except ValueError:
                    print('Вы ввели не число')
        if number == 8:
            print('Введите интервал для поиска пифагоровых троек')
            fl = True
            while fl:
                try:
                    n = int(input('Введите первое число: '))
                    m = int(input('Введите второе число: '))
                    if n >= m:
                        print('Первое число должно быть меньше')
                        continue
                    c = 0
                    for i in range(n, m + 1):
                        for j in range(n, m + 1):
                            if i <= j and ((i ** 2 + j ** 2) ** 0.5) % 1 == 0 and ((i ** 2 + j ** 2) ** 0.5) <= m:
                                c += 1
                                print(i, j, int((i ** 2 + j ** 2) ** 0.5))
                    if c == 0:
                        print('Пифагоровых троек в заданном интервале нет')
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 9:
            print('Введите интервал для поиска чисел которые делятся на каждую из своих цифр')
            fl = True
            while fl:
                try:
                    n = int(input('Введите первое число: '))
                    m = int(input('Введите второе число: '))
                    if n >= m:
                        print('Первое число должно быть меньше')
                        continue
                    for i in range(n, m + 1):
                        c = 0
                        for l in range(len(str(i))):
                            if int(str(i)[l]) and i % int(str(i)[l]) == 0:
                                c += 1
                            if c == len(str(i)):
                                print(i)
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 10:
            print(
                'Натуральное число называется совершенным, если оно равно сумме всех своих делителей, включая единицу. Вывести первые N (N<5) совершенных чисел на экран.')
            fl = True
            while fl:
                try:
                    n = int(input('Введите число: '))
                    if n < 1 or n > 5:
                        print('Введите число <5')
                        continue
                    c, chislo = 0, 1
                    while c < n:
                        summa = 0
                        for i in range(1, chislo):
                            if chislo % i == 0:
                                summa += i
                        if summa == chislo:
                            print(chislo)
                            c += 1
                        chislo += 1
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 11:
            print(
                'Задайте одномерный массив в коде и выведите в консоль последний элемент данного массива тремя способами.')
            fl = True
            while fl:
                try:
                    n = int(input('Введите длину массива: '))
                    m = [random.randint(1, 9) for i in range(n)]
                    print(m)
                    print('Первый способ: ', 'm[n-1] = ', m[n - 1])
                    print('Второй способ: ', 'm[-1] = ', m[-1])
                    print('Третий способ: ', 'm[len(m)-1] = ', m[len(m) - 1])
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 12:
            print('Задайте одномерный массив в коде и выведите в консоль массив в обратном порядке.')
            fl = True
            while fl:
                try:
                    n = int(input('Введите длину массива: '))
                    m = [random.randint(1, 9) for i in range(n)]
                    print(m)
                    print(m[-1::-1])
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 13:
            print('Реализуйте нахождение суммы элементов массива через рекурсию. Массив можно задать в коде.')
            fl = True
            def summ(m, i=0):
                if i >= len(m):
                    return 0
                return m[i] + summ(m, i + 1)
            while fl:
                try:
                    n = int(input('Введите длину массива: '))
                    m = [random.randint(1, 9) for i in range(n)]
                    print(m)
                    print(summ(m))
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 14:
            def rub_to_usd():
                try:
                    rubles = float(rub_entry.get())
                    dollars = rubles / 97.41
                    usd_label.config(text=f"Доллары: {dollars:.2f}")
                except ValueError:
                    messagebox.showerror("Ошибка", "Пожалуйста, введите число")


            def usd_to_rub():
                try:
                    dollars = float(usd_entry.get())
                    rubles = dollars * 97.41
                    rub_label.config(text=f"Рубли: {rubles:.2f}")
                except ValueError:
                    messagebox.showerror("Ошибка", "Пожалуйста, введите число")


            window = tk.Tk()
            window.title("Конвертер валют")
            window.geometry("300x150")
            rub_label = tk.Label(window, text="Рубли:")
            rub_label.pack()
            rub_entry = tk.Entry(window)
            rub_entry.pack()
            usd_label = tk.Label(window, text="Доллары:")
            usd_label.pack()
            usd_entry = tk.Entry(window)
            usd_entry.pack()
            rub_to_usd_button = tk.Button(window, text="Рубли в доллары", command=rub_to_usd)
            rub_to_usd_button.pack()
            usd_to_rub_button = tk.Button(window, text="Доллары в рубли", command=usd_to_rub)
            usd_to_rub_button.pack()
            window.mainloop()
        if number == 15:
            print(
                'Реализуйте вывод таблицы умножения в консоль размером n на m которые вводит пользователь, но при этом они не могут быть больше 20 и меньше 5.')
            fl = True
            while fl:
                try:
                    n = int(input('n = '))
                    m = int(input('m = '))
                    if n < 5 or n > 20 or m < 5 or m > 20:
                        print('Вводите числа в диапазоне от 5 до 20')
                        continue
                    for i in range(1, n + 1):
                        for j in range(1, m):
                            print(i * j, end='\t')
                        print(i * m)
                    fl = False
                except ValueError:
                    print('Вы ввели не число')
        if number == 16:
            field = [['-' for _ in range(12)] for _ in range(12)]


            def check_place(ship):
                fl = True
                for i in ship:
                    x, y = i
                    if 0 < x < 11 and 0 < y < 11:
                        for m in range(-1, 2):
                            for n in range(-1, 2):
                                if field[x + m][y + n] != '-':
                                    fl = False
                    else:
                        fl = False
                return fl


            def create_ship(tek_len):
                x = random.randint(1, 10)
                y = random.randint(1, 10)
                orientation = random.choice(['hor', 'ver'])
                ship = []
                if orientation == 'hor':
                    for i in range(tek_len):
                        x += 1
                        ship.append([x, y])
                else:
                    for i in range(tek_len):
                        y += 1
                        ship.append([x, y])
                return ship


            ships = []
            ships_len = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1]

            for tek_len in ships_len:
                ship = create_ship(tek_len)
                fl = check_place(ship)
                while not fl:
                    ship = create_ship(tek_len)
                    fl = check_place(ship)
                if fl:
                    ships.append(ship)
                    for ship in ships:
                        for k in ship:
                            x, y = k
                            field[x][y] = '0'

            for row in field[1:-1]:
                roww = row[1:-1]
                print(' '.join((roww)))

    except ValueError:
        print('Вводите правильный номер задачи!')


