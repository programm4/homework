import pygame
import math
pygame.init()

WHITE = (255, 255, 255)
YELLOW = pygame.transform.scale(pygame.image.load('images/sun.png'), (20, 20))
MERC = pygame.transform.scale(pygame.image.load('images/mercury.png'), (3.12, 3.12))
VEN = pygame.transform.scale(pygame.image.load('images/venera.png'), (7.75, 7.75))
EAR = pygame.transform.scale(pygame.image.load('images/earth.png'), (8.15, 8.15))
MAR = pygame.transform.scale(pygame.image.load('images/mars.png'), (4.33, 4.33))
JUP = pygame.transform.scale(pygame.image.load('images/jupiter.png'), (91.51, 91.51))
SAT = pygame.transform.scale(pygame.image.load('images/saturn.png'), (77.14*1.8, 77.14))
UR = pygame.transform.scale(pygame.image.load('images/uran.png'), (32.72*1.2, 32.72))
NEP = pygame.transform.scale(pygame.image.load('images/neptun.png'), (31.70, 31.70))
bg = pygame.image.load("images/nebo2.jpg")

names = ['Меркурий', 'Венера', 'Земля', 'Марс', 'Юпитер', 'Сатурн', 'Уран', 'Нептун']
diametr = ['Диаметр: 4 876 км', 'Диаметр: 12 103 км', 'Диаметр: 12 742 км', 'Диаметр: 6 794 км',
           'Диаметр: 142 984 км', 'Диаметр: 120 536 км', 'Диаметр: 51 118 км', 'Диаметр: 49 528 км']
udal = ['Удаленность: 58 млн км', 'Удаленность: 108 млн км', 'Удаленность: 149 млн км', 'Удаленность: 228 млн км',
        'Удаленность: 778 млн км', 'Удаленность: 1 429 млн км', 'Удаленность: 2,8 млрд км', 'Удаленность: 4,55 млрд км']
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Солнечная система")

class Planet:
    def __init__(self, name, size, distance, color, speed):
        self.name = name
        self.size = size
        self.distance = distance
        self.color = color
        self.speed = speed
        self.angle = 0
    def update(self):
        self.angle += self.speed
    def draw(self):
        x = int((SCREEN_WIDTH / 2) + math.cos(math.radians(self.angle)) * self.distance)
        y = int((SCREEN_HEIGHT / 2) + math.sin(math.radians(self.angle)) * self.distance)
        screen.blit(self.color, (x-self.size, y-self.size))

sun = Planet("Sun", 10, 0, YELLOW, 0)
mercury = Planet("Mercury", 1.95, 20, MERC, 4.091)
venus = Planet("Venus", 4.84, 25, VEN, 1.6)
earth = Planet("Earth", 5.09, 35, EAR, 0.96)
mars = Planet("Mars", 2.71, 45, MAR, 0.524)
jupiter = Planet("Jupiter", 57.19, 110, JUP, 0.083)
saturn = Planet("Saturn", 48.21, 210, SAT, 0.033)
uran = Planet("Uran", 20.45, 310, UR, 0.012)
neptun = Planet("Neptun", 19.81, 350, NEP, 0.006)

clock = pygame.time.Clock()
running = True
days_counter = 0
tek_pl = 0

while running:
    clock.tick(300)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_RIGHT]:
        tek_pl += 1
        if tek_pl > 7:
            tek_pl -= 8
    if keys[pygame.K_LEFT]:
        tek_pl -=1
        if tek_pl < 0:
            tek_pl += 8


    screen.blit(bg, (0, 0))
    mercury.update()
    venus.update()
    earth.update()
    mars.update()
    jupiter.update()
    saturn.update()
    uran.update()
    neptun.update()

    sun.draw()
    mercury.draw()
    venus.draw()
    earth.draw()
    mars.draw()
    jupiter.draw()
    saturn.draw()
    uran.draw()
    neptun.draw()

    days_counter += 1
    font = pygame.font.Font(None, 20)
    text = font.render(f"Earth Days: {days_counter}", True, WHITE)
    screen.blit(text, (10, 20))

    inf1 = font.render(names[tek_pl], True, WHITE)
    screen.blit(inf1, (10, 40))
    inf2 = font.render(diametr[tek_pl], True, WHITE)
    screen.blit(inf2, (10, 55))
    inf3 = font.render(udal[tek_pl], True, WHITE)
    screen.blit(inf3, (10, 70))

    pygame.display.flip()

pygame.quit()
