class Dot:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def line(self, other):
        return ((self.x - other.x)**2 + (self.y - other.y)**2)**0.5

class Triangle:
    def __init__(self, A, B, C):
        self.A = Dot(*A)
        self.B = Dot(*B)
        self.C = Dot(*C)
        self.ab = self.B.line(self.A)
        self.bc = self.C.line(self.B)
        self.ac = self.C.line(self.A)
    def checking_triangle(self):
        a, b, c = self.ab, self.ac, self.bc
        return a + b + c - max(a, b, c) > max(a, b, c)
    def square_triangle(self):
        a, b, c = self.ab, self.bc, self.ac
        p = (a + b + c)/2
        return (p*(p-a)*(p-b)*(p-c))**0.5

f = open('plist.txt')
points = f.readline().split(']')[:-1]
for i in range(len(points)):
    points[i] = points[i][1::]
points = [tuple(map(int, p.split(','))) for p in points]

min_triangle = 9999999999
max_triangle = 0
for i in range(len(points)-2):
    for j in range(i+1, len(points)-1):
        for l in range(j+1, len(points)):
            tek_triangle = Triangle(points[i], points[j], points[l])
            if tek_triangle.checking_triangle():
                tek_square = tek_triangle.square_triangle()
                if tek_square < min_triangle and tek_square > 0:
                    min_triangle = tek_square
                if tek_square > max_triangle:
                    max_triangle = tek_square
print('Минимальная площадь: ', min_triangle)
print('Максимальная площадь: ', max_triangle)
