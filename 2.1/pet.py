from threading import Timer
print('У вас появился новый домашний питомец. Вам надо заботиться о нем: кормить(F) и играть с ним(P), иначе он убежит. Удачи!')
class Pet:
    def __init__(self, feed=100, game=100):
        self.__feed = feed
        self.__game = game
        self.__timer = Timer(0, print)
    def play(self):
        self.__feed -= 20
        self.__game += 50
        if self.__game > 100:
            self.__game = 100
    def eat(self):
        self.__feed += 50
        if self.__feed > 100:
            self.__feed = 100
    def end(self):
        self.__timer.cancel()
    def life(self):
        self.__timer.cancel()
        self.__game -= 5
        self.__feed -= 10
        print('Настроение - ', self.__game, 'Сытость - ', self.__feed)
        if self.__feed <= 0 and self.__game <= 0:
            print('Мало того, что он голодный, он еще и грустный. Не удивительно что он ушел')
        elif self.__feed <= 0:
            print('Ваш зверек проголодался и ушел на поиски еды :(')
            self.__timer.cancel()
        elif self.__game <= 0:
            print('Ваш зверек впал в депрессию и убежал :((')
            self.__timer.cancel()
        elif self.__feed <= 20:
            ans = input('Кажется, ваш питонец проголодался. Покормите его? (Y/N)')
            if ans == 'Y':
                self.eat()
                self.life()
            else:
                self.life()
        elif self.__game <= 20:
            ans2 = input('Кажется, питомец загрустил. Поиграете с ним? (Y/N)')
            if ans2 == 'Y':
                self.play()
                self.life()
            else:
                self.life()
        else:
            self.__timer = Timer(1, self.life)
            self.__timer.start()

n = Pet()
while True:
    action = input()
    match action:
        case 'S':
            n.life()
        case 'F':
            n.eat()
        case 'P':
            n.play()
