import pygame
import random
import copy
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
size = (100+30*30, 100+15*30)
LETTERS = 'АБВГДЕЖЗИК'
pygame.init()
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Морской бой")
font = pygame.font.SysFont('roboto', 30)
font2 = pygame.font.SysFont('roboto', 100)
computer_free_blocks = set((a, b) for a in range(1, 11) for b in range(1, 11))
last_computer_hit = set()
hit_blocks = set()
dots = set()
dots_for_comp = set()
died_human_blocks = set()
last_hits_list = []
died_ships = []
class ShipsOnGrid:
    def __init__(self):
        self.available_blocks = set((a, b) for a in range(1, 11) for b in range(1, 11))
        self.ships_set = set()
        self.ships = self.populate_grid()
    def create_ship(self, number_of_blocks, available_blocks):
        ship_coordinates = []
        x_or_y = random.randint(0, 1)
        str_rev = random.choice((-1, 1))
        x, y = random.choice(tuple(available_blocks))
        for _ in range(number_of_blocks):
            ship_coordinates.append((x, y))
            if not x_or_y:
                if (x <= 1 and str_rev == -1) or (x >= 10 and str_rev == 1):
                    str_rev *= -1
                    str_rev, x = str_rev, ship_coordinates[0][x_or_y] + str_rev
                else:
                    str_rev, x = str_rev, ship_coordinates[-1][x_or_y] + str_rev
            else:
                if (y <= 1 and str_rev == -1) or (y >= 10 and str_rev == 1):
                    str_rev *= -1
                    str_rev, y = str_rev, ship_coordinates[0][x_or_y] + str_rev
                else:
                    str_rev, y = str_rev, ship_coordinates[-1][x_or_y] + str_rev
        if set(ship_coordinates).issubset(self.available_blocks):
            return ship_coordinates
        return self.create_ship(number_of_blocks, available_blocks)
    def populate_grid(self):
        ships_coordinates_list = []
        for number_of_blocks in range(4, 0, -1):
            for _ in range(5-number_of_blocks):
                new_ship = self.create_ship(
                    number_of_blocks, self.available_blocks)
                ships_coordinates_list.append(new_ship)
                for elem in new_ship:
                    self.ships_set.add(elem)
                for elem in new_ship:
                    for k in range(-1, 2):
                        for m in range(-1, 2):
                            if 0 < (elem[0] + k) < 11 and 0 < (elem[1] + m) < 11:
                                self.available_blocks.discard((elem[0] + k, elem[1] + m))
        return ships_coordinates_list
computer = ShipsOnGrid()
human = ShipsOnGrid()
computer_ships_working = copy.deepcopy(computer.ships)
human_ships_working = copy.deepcopy(human.ships)
def check_win(ship_list):
    for elem in ship_list:
        if len(elem) != 0:
            return False
    return True
def draw_ships(ships_coor):
    for elem in ships_coor:
        ship = sorted(elem)
        x_start = ship[0][0]
        y_start = ship[0][1]
        if len(ship) > 1 and ship[0][0] == ship[1][0]:
            ship_width = 30
            ship_height = 30 * len(ship)
        else:
            ship_width = 30 * len(ship)
            ship_height = 30
        x = 30 * (x_start - 1) + 100
        y = 30 * (y_start - 1) + 100
        if ships_coor == human.ships:
            x += 15 * 30
        pygame.draw.rect(screen, BLACK, ((x, y), (ship_width, ship_height)), 3)
def draw_grid():
    for i in range(11):
        for j in (0, 450):
            pygame.draw.line(screen, BLACK, (100 + j, 100 + i*30), (400 + j, 100 + i*30))
            pygame.draw.line(screen, BLACK, (100 + i*30 + j, 100), (100 + i*30 + j, 400))
            if i < 10:
                number = font.render(str(i+1), 1, BLACK)
                letter = font.render(LETTERS[i], 1, BLACK)
                screen.blit(number, (70 + j, 105 + i*30))
                screen.blit(letter, (105 + i*30 + j, 75))
    player1 = font.render("Компьютер", True, BLACK)
    player2 = font.render("Человек", True, BLACK)
    screen.blit(player1, (200, 30))
    screen.blit(player2, (630, 30))
def computer_shoots(set_to_shoot_from):
    pygame.time.delay(500)
    computer_fired_block = random.choice(tuple(set_to_shoot_from))
    computer_free_blocks.discard(computer_fired_block)
    return check_step(computer_fired_block, human_ships_working, True)
def check_step(fired_block, opponents_ships_list, who_do_step, diagonal_only=True):
    for elem in opponents_ships_list:
        if fired_block in elem:
            update_steps(
                fired_block, who_do_step, diagonal_only=True)
            ind = opponents_ships_list.index(elem)

            if len(elem) == 1:
                update_steps(
                    fired_block, who_do_step, diagonal_only=False)

            elem.remove(fired_block)

            if who_do_step:
                last_hits_list.append(fired_block)
                human.ships_set.discard(fired_block)
                update_last_died(fired_block)
            else:
                computer.ships_set.discard(fired_block)

            if not elem:
                draw_died_ships(ind, opponents_ships_list, who_do_step)
                if who_do_step:
                    last_hits_list.clear()
                    last_computer_hit.clear()
                else:
                    died_ships.append(computer.ships[ind])

            return True
    dot_on_block(fired_block, who_do_step)
    if who_do_step:
        update_last_died(fired_block, False)
def dot_on_block(fired_block, who_do_step=False):
    if not who_do_step:
        dots.add(fired_block)
    else:
        dots.add((fired_block[0] + 15, fired_block[1]))
        dots_for_comp.add(fired_block)
def update_last_died(fired_block, computer_hits=True):
    global last_computer_hit, computer_free_blocks
    if computer_hits and fired_block in last_computer_hit:
        new_around_last_hit_set = set()
        for i in range(len(last_hits_list)-1):
            if last_hits_list[i][0] == last_hits_list[i+1][0]:
                if 1 < last_hits_list[i][1]:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0], last_hits_list[i][1] - 1))
                if 1 < last_hits_list[i+1][1]:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0], last_hits_list[i+1][1] - 1))
                if last_hits_list[i][1] < 10:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0], last_hits_list[i][1] + 1))
                if last_hits_list[i+1][1] < 10:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0], last_hits_list[i+1][1] + 1))

            elif last_hits_list[i][1] == last_hits_list[i+1][1]:
                if 1 < last_hits_list[i][0]:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0] - 1, last_hits_list[i][1]))
                if 1 < last_hits_list[i+1][0]:
                    new_around_last_hit_set.add(
                        (last_hits_list[i+1][0] - 1, last_hits_list[i][1]))
                if last_hits_list[i][0] < 10:
                    new_around_last_hit_set.add(
                        (last_hits_list[i][0] + 1, last_hits_list[i][1]))
                if last_hits_list[i+1][0] < 10:
                    new_around_last_hit_set.add(
                        (last_hits_list[i+1][0] + 1, last_hits_list[i][1]))

        last_computer_hit = new_around_last_hit_set

    elif computer_hits and fired_block not in last_computer_hit:
        xhit, yhit = fired_block
        if 1 < xhit:
            last_computer_hit.add((xhit-1, yhit))
        if 1 < yhit:
            last_computer_hit.add((xhit, yhit-1))
        if xhit < 10:
            last_computer_hit.add((xhit+1, yhit))
        if yhit < 10:
            last_computer_hit.add((xhit, yhit+1))

    elif not computer_hits:
        last_computer_hit.discard(fired_block)

    last_computer_hit -= dots_for_comp
    last_computer_hit -= died_human_blocks
    computer_free_blocks -= last_computer_hit
    computer_free_blocks -= dots_for_comp
def update_steps(fired_block, who_do_step, diagonal_only=True):
    global dots
    x, y = fired_block
    a, b = 0, 11
    if who_do_step:
        x += 15
        a += 15
        b += 15
        died_human_blocks.add(fired_block)
    hit_blocks.add((x, y))
    for i in range(-1, 2):
        for j in range(-1, 2):
            if diagonal_only:
                if i != 0 and j != 0 and a < x + i < b and 0 < y + j < 11:
                    dots.add((x+i, y+j))
                    if who_do_step:
                        dots_for_comp.add(
                            (fired_block[0]+i, y+j))
            else:
                if a < x + i < b and 0 < y + j < 11:
                    dots.add((x+i, y+j))
                    if who_do_step:
                        dots_for_comp.add((
                            fired_block[0]+i, y+j))
    dots -= hit_blocks
def draw_died_ships(ind, opponents_ships_list, who_do_step, diagonal_only=False):
    if opponents_ships_list == computer_ships_working:
        ships_list = computer.ships
    elif opponents_ships_list == human_ships_working:
        ships_list = human.ships
    ship = sorted(ships_list[ind])
    for i in range(-1, 1):
        update_steps(ship[i], who_do_step, diagonal_only)
def draw_dots(dots):
    for elem in dots:
        pygame.draw.circle(screen, BLACK, (30*(
            elem[0]-0.5)+100, 30*(elem[1]-0.5)+100), 30//6)
def draw_died_blocks(hit_blocks):
    for block in hit_blocks:
        x1 = 30 * (block[0]-1) + 100
        y1 = 30 * (block[1]-1) + 100
        pygame.draw.line(screen, BLACK, (x1, y1),(x1+30, y1+30), 30//6)
        pygame.draw.line(screen, BLACK, (x1, y1+30),(x1+30, y1), 30//6)
def main():
    game_over = False
    who_do_step = False

    screen.fill(WHITE)
    draw_grid()
    draw_ships(human.ships)
    # draw_ships(computer.ships)
    pygame.display.update()

    while not game_over:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_over = True
            elif not who_do_step and event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos

                if (100 <= x <= 100 + 10*30) and (100 <= y <= 100 + 10*30):
                    fired_block = ((x - 100) // 30 + 1, (y - 100) // 30 + 1)

                who_do_step = not check_step(fired_block, computer_ships_working, who_do_step)

        if who_do_step:
            if last_computer_hit:
                who_do_step = computer_shoots(last_computer_hit)
            else:
                who_do_step = computer_shoots(computer_free_blocks)

        draw_dots(dots)
        draw_died_blocks(hit_blocks)
        draw_ships(died_ships)
        pygame.display.update()

        if check_win(human_ships_working):
            pygame.draw.rect(screen, WHITE, (200, 430, 550, 100))
            text = font2.render('Вы проиграли(', 1, BLACK)
            screen.blit(text, (220, 450))
            draw_ships(computer.ships)

        elif check_win(computer_ships_working):
            pygame.draw.rect(screen, WHITE, (200, 430, 500, 100))
            text = font2.render('Вы выиграли!', 1, BLACK)
            screen.blit(text, (220, 450))


main()
pygame.quit()
