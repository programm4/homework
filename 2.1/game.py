import pygame
import random

BLUE = (58, 87, 156)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (87, 183, 125)

block_size = 30
left_margin = 100
top_margin = 100



LETTERS = 'АБВГДЕЖЗИК'

pygame.init()

screen = pygame.display.set_mode((920, 500))
pygame.display.set_caption('Морской бой')

font = pygame.font.SysFont('Roboto', 30)
font_caption = pygame.font.SysFont('Roboto', 70)

player1 = font.render('Игрок 1', 1, BLACK)
player2 = font.render('Игрок 2', 1, BLACK)


class ShipsPlaces:
    def __init__(self):
        self.free_blocks = set((a, b) for a in range(1, 11) for b in range(1, 11))
        self.ships_list = set()
        self.ships = self.all_ships_coor()

    def checking_ship_valid(self, new_ship): #не задевает ли корабли другие корабли
        ship = set(new_ship)
        return ship.issubset(self.free_blocks)

    def create_ship(self, len_ship, free_blocks):
        ship_coor = []
        hor_vert = random.choice((0, 1))
        front_back = random.choice((-1, 1))
        x, y = random.choice(tuple(free_blocks))
        for i in range (len_ship):
            ship_coor.append((x, y))
            if hor_vert == 0:
                if (x <= 1 and hor_vert == -1) or (x>=10 and hor_vert == 1):
                    front_back *= -1
                    x = ship_coor[0][0] + front_back
                else:
                    x = ship_coor[-1][0] + front_back
            else:
                if (y <= 1 and front_back == -1) or (y >= 10 and front_back == 1):
                    front_back *= -1
                    y = ship_coor[0][1] + front_back
                else:
                    y = ship_coor[-1][1] + front_back
        if self.checking_ship_valid(ship_coor):
            return ship_coor
        return self.create_ship(len_ship, free_blocks)

    def all_ships_coor(self):
        all_ships = []
        for len_ship in range(4, 0, -1):
            for i in range(5-len_ship):
                new_ship = self.create_ship(len_ship, self.free_blocks)
                all_ships.append(new_ship)
                for el in new_ship:
                    self.ships_list.add(el)
                    for k in range(-1, 2):
                        for m in range(-1, 2):
                            if 0 < (el[0] + k) < 11 and 0 < (el[1] + m) < 11:
                                self.free_blocks.discard((el[0]+ k, el[1] + m))
        return all_ships

first_player = ShipsPlaces()
second_player = ShipsPlaces()


def draw_ships(all_ships):
    # print('dfghjdfghnjm')
    for el in all_ships:
        ship = sorted(el)
        x_start = ship[0][0]
        y_start = ship[0][1]
        if len(ship) > 1 and ship[0][0] == ship[1][0]:
            ship_width = 30
            ship_height = 30 * len(ship)
        else:
            ship_height = 30
            ship_width = 30 * len(ship)
        x = 30 * (x_start - 1) + 100
        y = 30 * (y_start - 1) + 100
        # if all_ships == second_player.ships:
        #     x = x + 400
        pygame.draw.rect(screen, BLACK, ((x, y), (ship_width, ship_height)), 3)


class Button:
    def __init__(self, width, height):
        self.width = width
        self.height = height

    def draw(self, x, y, message):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        if x < mouse[0] < x + self.width and y < mouse[1] < y + self.height:
            pygame.draw.rect(screen, GREEN, (x, y, self.width, self.height))
            if click[0] == 1:
                pygame.time.delay(300)
                return True
        else:
            pygame.draw.rect(screen, BLUE, (x, y, self.width, self.height))

        text = font.render(message, 1, BLACK)
        screen.blit(text, (x + 10, y + 10))
        return False



def draw_start_window():
    caption = font_caption.render('Морской бой', 1, BLACK)
    screen.blit(caption, (300, 100))
    button = Button(150, 50)
    if button.draw(400, 200, 'Начать игру') > 0:
        return True
    return False

def draw_grid():
    screen.fill(WHITE)

    screen.blit(player1, (200, 30))
    screen.blit(player2, (630, 30))
    for i in range(11):
        for j in (0, 420):
            #клеточки
            pygame.draw.line(screen, BLUE, (100 + j, 100 + i*30), (400 + j, 100 + i*30))
            pygame.draw.line(screen, BLUE, (100 + i*30 + j, 100), (100 + i*30 + j, 400))
            #циферки и буковки
            if i < 10:
                number = font.render(str(i+1), 1, BLACK)
                letter = font.render(LETTERS[i], 1, BLACK)
                screen.blit(number, (70 + j, 105 + i*30))
                screen.blit(letter, (105 + i*30 + j, 75))

def draw_grid_to_player(player, player_ships):
    screen.fill(WHITE)
    # player1 = font.render('Игрок 1', 1, BLACK)
    screen.blit(player, (200, 30))
    for i in range(11):
            #клеточки
        pygame.draw.line(screen, BLUE, (100 , 100 + i*30), (400, 100 + i*30))
        pygame.draw.line(screen, BLUE, (100 + i*30, 100), (100 + i*30, 400))
        #циферки и буковки
        if i < 10:
            number = font.render(str(i+1), 1, BLACK)
            letter = font.render(LETTERS[i], 1, BLACK)
            screen.blit(number, (70, 105 + i*30))
            screen.blit(letter, (105 + i*30, 75))
    # for i in range(4, 0, -1):
    #     for j in range(5-i):
    #         pygame.draw.rect(screen, BLACK, (450 + 30 * (i+1) * j, 100 + 50*(4-i), 30 * i, 30), 5)

    # button_auto = Button(220, 45)
    # button_auto.draw(450, 300, 'Pасставить корабли')
    draw_ships(player_ships.ships)
    # button_reverse = Button(140, 50)
    # button_reverse.draw(500, 370, 'Повернуть')
    button_next = Button(140, 45)
    if button_next.draw(450, 360, 'Готово') > 0:
        return True
    return False

def main():

    game_running = True
    screen.fill(WHITE)
    start_game = False
    first_player_ready = False
    # first_player_ships = False
    second_player_ready = False
    # game_started = False
    while game_running:
        # draw_ships(first_player.ships)
        # pygame.draw.rect(screen, GREEN, (0, 0, 100, 100))
        # pygame.draw.rect(screen, GREEN, (0, 0, 520, 100))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game_running = False
            if start_game == False:
                if draw_start_window():
                    start_game = True
            elif first_player_ready == False:
                if draw_grid_to_player(player1, first_player):
                    first_player_ready = True
            elif second_player_ready == False:
                if draw_grid_to_player(player2, second_player):
                    second_player_ready = True
            else:
                draw_grid()

        pygame.display.update()
main()
pygame.quit()

