from tkinter import *
f = open("questions.txt", encoding="utf-8").readlines()

def quiz():
    global number, tek_quest, ans, frame1, button
    m = questions[0]
    tek_quest = Label(frame, text=m, font="Arial 12", height=3)
    tek_quest.pack()
    questions.remove(m)
    frame1 = Frame(root)
    frame1.pack(expand=True)
    num = -1
    for i in answers[:3]:
        num += 1
        ans = Radiobutton(frame1, width=30, text=i, font="Arial 12", variable=var, value=num)
        ans.pack(padx=10, pady=10)
        answers.remove(i)
    button = Button(text="Ответить", font="Arial 12", command=choices)
    button.pack(padx=10, pady=30)

def choices():
    global score, number, result, proc
    number += 1
    for j in answer2[:3]:
        if var.get() == answer2.index(j) and j in right_ans:
            score += 1

    if number < 10:
        tek_quest.destroy()
        for i in range(3):
            frame1.destroy()
            button.destroy()
        quiz()
    else:
        tek_quest.destroy()
        for i in range(3):
            frame1.destroy()
            frame.destroy()
            button.destroy()
        result = Label(root, text=f"Результат: {score}/10", font="Arial 12", width=50, height= 10)
        result.pack(expand=True)

root = Tk()
root.title('Тест по Солнечной системе')
root.geometry('700x400')

frame = Frame(root)
frame.pack(expand=False)

score = 0

right_ans = []
questions = []
answers = []
for line in f:
    if line[:2]=="q:":
        questions.append(line[2:])
    elif line[0]=="+":
        right_ans.append(line[1:])
        answers.append(line[1:])
    else:
        answers.append(line)
answer2 = answers.copy()
number = 0
var = IntVar()
var.set(0)
quiz()
root.mainloop()
