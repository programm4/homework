import pygame

screen = pygame.display.set_mode((300, 300))
rect = pygame.Rect(20, 20, 20, 20)

colors = ["#FFFFFF", "#9702A7", "#FF0000", "#FF6D94", "#5949DE", "#FFEC3A", "#FF833A"]
base_color = (255, 255, 255)
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
                for i in colors:
                    base_color  = i
                    colors.remove(i)
                    colors.append(i)
                    break
            elif event.key == pygame.K_LEFT:
                if rect.left < 20:
                    break
                else:
                    rect.move_ip(-20, 0)
            elif event.key == pygame.K_RIGHT:
                if rect.right > 280:
                    break
                else:
                    rect.move_ip(20, 0)
            elif event.key == pygame.K_UP:
                if rect.top < 20:
                    break
                rect.move_ip(0, -20)
            elif event.key == pygame.K_DOWN:
                if rect.bottom > 280:
                    break
                else:
                    rect.move_ip(0, 20)

    pygame.draw.rect(screen, base_color , rect, 0)

    pygame.display.flip()
