import turtle

def Pattern(l):
    for x in range(24):
        t.pencolor(colors[x % 6])
        t.width(x / 10 + 1)

        t.penup()
        t.home()
        t.right(90)
        t.fd(50 + x * 25 *l)
        t.left(90)
        t.pendown()
        t.left(135)
        t.fd(42 * x*l)
        t.right(45)
        t.fd(20 * x*l)
        t.right(45)
        t.fd(14 * x*l)
        t.right(45)
        t.fd(10 * x*l)
        t.right(45)
        t.fd(14 * x*l)

        t.left(90)
        t.fd(14 * x*l)
        t.right(45)
        t.fd(10 * x*l)
        t.right(45)
        t.fd(14 * x*l)
        t.right(45)
        t.fd(20 * x*l)
        t.right(45)
        t.fd(42 * x*l)

screen = turtle.Screen()
screen.tracer(0)
t = turtle.Pen()
#turtle.speed(10)
turtle.delay(0)
turtle.bgcolor('black')
t.hideturtle()
colors=['#EE6B9C', 'pink', '#900035', 'red', 'purple', '#DE0052']
l = 0.1
v = 0.001
for g in range(10000):
    t.clear()
    Pattern(l)
    screen.update()
    t.home()
    l +=v
    if l>1 or l<0.01:
        v=-v



turtle.exitonclick()
