file = open("first.txt")
first = []
for i in file.readlines():
    first.append(int(i[:-1]))
print("Первая коллекция:", first)
file = open("second.txt")
second= []
for i in file.readlines():
    second.append(int(i[:-1]))
print("Вторая коллекция:", second)
file = open("third.txt")
third = []
for i in file.readlines():
   third.append(int(i[:-1]))
print("Третья коллекция:", third)
#1
print('Удвоить каждый эелемент коллекции:', sum(map(lambda x: [x, x*2], first), []))
#2
print('Найти произведение по-эементно элементов из трёх коллекций:', sum(map(lambda x, y, z: [x*y*z], first, second, third), []))
#3
print('Найти длину каждого элемента коллекции:', list(zip(first, map(lambda x: len(str(x)), first))))
#4
print('Оставить только четные элементы коллекции:', list(filter(lambda x: not int(x) % 2, first)))
#5
print('Оставить только непустые элементы коллекции:', list(filter(None, third)))
#6
print('Есть три коллекции, нужно упаковать элементы тройками:', list(zip(first, second, third)))
#7
print('Есть две коллекции, нужно упаковать элементы двойками, при том элементы второй коллекции должны быть удвоенны:', list(zip(first, sum(map(lambda x: [x*2], second), []))))
