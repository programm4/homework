import pygame
import sys
import random
from tkinter import *
from tkinter.messagebox import askyesno
from tkinter import ttk

pygame.mixer.init()
pygame.init()

apple_sound = pygame.mixer.Sound('apples.mp3')
over_sound = pygame.mixer.Sound('losing_game.mp3')
pygame.mixer.music.load('fon_music.mp3')
pygame.mixer.music.play(-1)



screen = pygame.display.set_mode((450, 500))
screen.fill("#FBF284")
pygame.display.set_caption('Змейка')
timer = pygame.time.Clock()
courier = pygame.font.SysFont('courier', 36)

class Snake:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def confict(self):
        return 0 <= self.x < 20 and 0 <= self.y < 20

    def __eq__(self, delta):
        return isinstance(delta, Snake) and self.x == delta.x and self.y == delta.y


def window():
    result = askyesno(title="Game over", message=f'Вы проиграли!\nВаш результат: {total}\nПродолжить игру?')
    if result:
        pygame.mixer.music.play(-1)
        return result
    else:
        pygame.quit()
        sys.exit()


def get_random_empty_block():
    x = random.randint(0, 20 - 1)
    y = random.randint(0, 20 - 1)
    empty_block = Snake(x, y)
    while empty_block in snake_blocks:
        empty_block.x = random.randint(0, 20 - 1)
        empty_block.y = random.randint(0, 20 - 1)
    return empty_block
def drow_block(color, row, column):
    pygame.draw.rect(screen, color, [20 + column * 20,
                                     50 + 20 + row * 20, 20,
                                     20])

snake_blocks = [Snake(9, 8), Snake(9, 9), Snake(9, 10)]
apple = get_random_empty_block()
d_row = buf_row = 0
d_col = buf_col = 1
total = 0
speed = 1

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP and d_col != 0:
                buf_row = -1
                buf_col = 0
            elif event.key == pygame.K_DOWN and d_col != 0:
                buf_row = 1
                buf_col = 0
            elif event.key == pygame.K_LEFT and d_row != 0:
                buf_row = 0
                buf_col = -1
            elif event.key == pygame.K_RIGHT and d_row != 0:
                buf_row = 0
                buf_col = 1



    pygame.draw.rect(screen, "#FBF284", [0, 0, 450, 50])
    text_total = courier.render(f"Score: {total}", 0, "#000000")
    screen.blit(text_total, (20, 20))

    for row in range(20):
        for column in range(20):
            drow_block("#E7DE78", row, column)

    head = snake_blocks[-1]
    if not head.confict():
        pygame.mixer.music.stop()
        over_sound.play()
        window()
        snake_blocks = [Snake(9, 8), Snake(9, 9), Snake(9, 10)]
        head = snake_blocks[-1]
        d_row = buf_row = 0
        d_col = buf_col = 1
        total = 0

    drow_block("#FF0700", apple.x, apple.y)
    for block in snake_blocks:
        drow_block("#5054FF", block.x, block.y)

    if apple == head:
        total += 1
        speed = total // 5 + 1
        snake_blocks.append(apple)
        apple = get_random_empty_block()
        pygame.mixer.Sound.play(apple_sound)

    d_row = buf_row
    d_col = buf_col
    new_head = Snake(head.x + d_row, head.y + d_col)

    if new_head in snake_blocks:
        pygame.mixer.music.stop()
        over_sound.play()
        window()
        snake_blocks = [Snake(9, 8), Snake(9, 9), Snake(9, 10)]
        head = snake_blocks[-1]
        d_row = buf_row = 0
        d_col = buf_col = 1
        new_head = Snake(head.x + d_row, head.y + d_col)
        total = 0

    snake_blocks.append(new_head)
    snake_blocks.pop(0)
    pygame.display.flip()
    timer.tick(3)

pygame.quit()
sys.exit()
