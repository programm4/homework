import os
path = input("Введите путь к папке:")
filelist = []
for root, dirs, files in os.walk(path):
	for file in files:
		filelist.append(os.path.join(root,file))
text_fail = 0
graf_fail = 0
tablis_fail = 0
for name in filelist:
    if name[-5:] == ".dock" or name[-4:] == ".txt" or name[-4:] == ".dat":
        text_fail+=1
    if name[-4:] == ".jpg" or name[-4:] == ".png" or name[-5:] == ".jpeg" or name[-4:] == ".gif":
        graf_fail+=1
    if name[-5:] == ".xlsx":
        tablis_fail+=1
print("Количество текстовых файлов", text_fail)
print("Количество графических файлов", graf_fail)
print("Количество табличных файлов", tablis_fail)
