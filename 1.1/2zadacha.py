try:
    n = int(input("Введите количество строк: "))
    m = int(input("Введите количество столбцов: "))
    k = int(input("Введите количество мин: "))
    if k>m*n:
        print("Данные введены неверно")
    pole=[["." for j in range(m)] for i in range(n)]
    #pole=["."(m+2)](n+2)
    for c in range (k):
        a, b = map(int, input("Введите координаты мины: ").split(" "))
        while pole[a-1][b-1] == "*":
            a, b = map(int, input("Есть такая мина. Введите новые координаты мины: ").split(" ")) 
        pole[a-1][b-1] ="*"
        for i in range(max(0,a-2), min(n,a+1)):
            for j in range(max(0,b-2), min(m,b+1)):
                if pole[i][j] == ".":
                    pole[i][j] = 1
                else:
                    if type(pole[i][j]) == int:
                        pole[i][j] +=1
    for i in range (0, n):
        print("".join([str(pole[i][j]) for j in range(m)]))
except ValueError:
    print("Данные введены неверно")
